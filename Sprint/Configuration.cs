﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sprint {
    public class Configuration {
        private static Configuration configuration = null;
        private static bool isToggleMode;
        
        public static Configuration getInstance()
        {
            if (configuration == null)
            {
                configuration = new Configuration();
            }
            return configuration;
        }

        private Configuration()
        {
            string line;
            string[] options;

            var path = @"./QMods/Sprint/config.txt";

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            options = line.Split('=');

            if (options[1].Equals("false"))
            {
                isToggleMode = false;
            }
            else
            {
                isToggleMode = true;
            }
        }

        public bool getToggleMode()
        {
            return isToggleMode;
        }
    }
}
